import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.util.ArrayList;

public class LogIn extends JFrame{
	private JTextField email = new JTextField(15);
	private JPasswordField password = new JPasswordField(15);
	private JLabel emailText = new JLabel("Email");
	private JLabel passwordText = new JLabel("Password");
	private JPanel panel = new JPanel();
	private JButton logIn = new JButton("Iniciar sesion");
	private ArrayList<User> users = new ArrayList<User>();
	private ArrayList<Item> stock = new ArrayList<Item>();
	private Home home;

	public LogIn(Store store){
		this.users = store.getUsers();
		this.stock = store.getStock();
		this.setTitle("Iniciar Sesion");
		this.setSize(400, 700);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.layOut();

		this.setVisible(true);
	}

	private void layOut(){
		this.setLayout(new BorderLayout());

		email.setFont(new Font("Serif", Font.PLAIN, 18));
		password.setFont(new Font("Serif", Font.PLAIN, 18));

		emailText.setFont(new Font("Courier", Font.PLAIN, 18));
		passwordText.setFont(new Font("Courier", Font.PLAIN, 18));

		logIn.addActionListener(new ActionListener(){
		
			@Override
			public void actionPerformed(ActionEvent e) {
				if (check()){
					JOptionPane.showMessageDialog(null, "Correto", "Inicio", JOptionPane.DEFAULT_OPTION);
					home = new Home(getStock());
				} else {
					JOptionPane.showMessageDialog(null, "incorreto", "Inicio", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		panel.setLayout(new GridBagLayout());
		GridBagConstraints gb = new GridBagConstraints();
		gb.fill = GridBagConstraints.HORIZONTAL;

		gb.gridx = 0;
		gb.gridy = 0;
		panel.add(emailText, gb);

		gb.gridx = 1;
		gb.gridy = 0;
		panel.add(email, gb);

		gb.gridx = 0;
		gb.gridy = 1;
		panel.add(passwordText, gb);

		gb.gridx = 1;
		gb.gridy = 1;
		panel.add(password, gb);

		gb.gridx = 0;
		gb.gridy = 2;
		gb.gridwidth = 2;
		panel.add(logIn, gb);

		this.getContentPane().add(panel, BorderLayout.CENTER);
	}

	private boolean check(){
		User userTemp = new User("#", "#", 0, "#");
		
		for(int i = 0; i < users.size(); i++){
			userTemp = users.get(i);

			if(userTemp.getEmail().equals(this.email.getText().toString()) && userTemp.getPassword().equals(new String(this.password.getPassword()))){
				return true;
			}	
		}
		return false;
	}

	private ArrayList<Item> getStock(){
		return this.stock;
	}
}