public class Item{
	private Product product;
	private int stock;

	public Item(Product product, int stock){
		this.product = product;
		this.stock = stock;
	}

	public Product getProduct(){
		return this.product; 
	}
	public int getStock(){
		return this.stock;
	}
}