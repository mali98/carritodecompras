import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.util.ArrayList;

public class Home extends JFrame{
	private JTextField email = new JTextField(15);
	private JPasswordField password = new JPasswordField(15);
	private JLabel emailText = new JLabel("Email");
	private JLabel passwordText = new JLabel("Password");
	private JPanel panel = new JPanel();
	private JButton logIn = new JButton("Iniciar sesion");
	private ArrayList<Item> stock = new ArrayList<Item>();

	public Home(ArrayList<Item> stock){
		this.stock = stock;
		this.setTitle("Store");
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.layOut();

		this.setVisible(true);
	}

	private void layOut(){
		this.setLayout(new BorderLayout());

		email.setFont(new Font("Serif", Font.PLAIN, 18));
		password.setFont(new Font("Serif", Font.PLAIN, 18));

		emailText.setFont(new Font("Courier", Font.PLAIN, 18));
		passwordText.setFont(new Font("Courier", Font.PLAIN, 18));

		logIn.addActionListener(new ActionListener(){
		
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});

		panel.setLayout(new GridBagLayout());
		GridBagConstraints gb = new GridBagConstraints();
		gb.fill = GridBagConstraints.HORIZONTAL;

		gb.gridx = 0;
		gb.gridy = 0;
		panel.add(new JLabel(stock.get(0).getProduct().getName()), gb);

		gb.gridx = 1;
		gb.gridy = 0;
		panel.add(new JLabel(stock.get(0).getProduct().getBrand()), gb);

		gb.gridx = 0;
		gb.gridy = 1;
		panel.add(new JLabel(stock.get(0).getProduct().getDescription()), gb);

		gb.gridx = 0;
		gb.gridy = 2;
		panel.add(new JLabel(stock.get(0).getProduct().getPrice().toString()), gb);

		this.getContentPane().add(panel, BorderLayout.CENTER);
	}
}