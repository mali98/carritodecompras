public class Product{
	private String name;
	private String brand;
	private String description;
	private Double price;

	public Product(String name, String brand, String description, Double price){
		this.name = name;
		this.brand = brand;
		this.description = description;
		this.price = price;
	}

	public String getName(){
		return this.name;
	}

	public String getDescription(){
		return this.description;
	}

	public Double getPrice(){
		return this.price;
	}

	public String getBrand(){
		return this.brand;
	}
}