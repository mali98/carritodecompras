public class User{
	private String name;
	private String password;
	private int age;
	private String email;
	private ShoppingCar shopCar;

	public User(String name, String password, int age, String email){
		this.name = name;
		this.password = password;
		this.age = age;
		this.email = email;
	}

	public String getName(){
		return this.name;
	}

	public String getPassword(){
		return this.password;
	}

	public int getAge(){
		return this.age;
	}

	public String getEmail(){
		return this.email;
	}

	public ShoppingCar getShopCar(){
		return this.shopCar;
	}

	public void setShopCar(ShoppingCar shopCar){
		this.shopCar = shopCar;
	}
}