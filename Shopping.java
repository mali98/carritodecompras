import javax.swing.*;
import java.awt.Container;
import java.util.ArrayList;

public class Shopping{
    private JFrame frame;
    private Container contentPane;
    private JButton btn;

    public void startShoppingCar(User user){
        doFrame(user.getName());
        contentPane = frame.getContentPane();
        setElements(user);
        frame.setVisible(true);

    }

    private void doFrame(String name){
        frame = new JFrame("Shopping Car of "+name);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setBounds(100, 20, 200, 100);  
        frame.setSize(1000, 700);
        frame.setResizable(false);
        
    }

    public void setElements(User user){
        showCarProducts(user.getShopCar());
        JButton b= new JButton("Regresar");
        contentPane.add(b);
    }

    public void showCarProducts(ShoppingCar sc){
        
    }
}
