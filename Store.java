import java.util.ArrayList;

public class Store{
	//private ArrayList<Item> item;
	//private ArrayList<Item>	item;
	private ArrayList<User> user;
	private ArrayList<Item>	item;

	public Store(){
		item = new ArrayList<Item>();
		item.add(new Item(new Product("VivoBook F510UA", "Asus", "Intel Core i5-8250U, 8GB RAM, 1TB HDD, USB-C", 11638.55), 15));
		item.add(new Item(new Product("Aspire E 15", "Acer", "15.6 Full HD, 8th Gen Intel Core i3-8130U, 6 GB de Memoria RAM, 1TB HDD", 8573.13), 20));
		item.add(new Item(new Product("Envy 13-ah0003la", "HP", "Intel Core i5-8250U 1.6GHz, 8GB RAM, 256GB SSD, Windows 10", 16899.00), 16));
		item.add(new Item(new Product("Iphone x", "Apple", "iOS 11, 64 GB de almacenamiento, Cámara: 12MP Dual / Frontal 7 MP", 23993.00), 24));
		item.add(new Item(new Product("Galaxy S9", "Samsung", "64 GB de almacenamiento, Cámara: 12MP / Frontal 8 MP", 13499.00), 34));
		//System.out.println	item.get(0).getStock());
		//System.out.println	item.get(0).getProduct().getName());

		for (int i = 0; i <	item.size(); i++){
			System.out.println(item.get(i).getStock() + "\t" +	item.get(i).getProduct().getName());
		}

		user = new ArrayList<User>();
		user.add(new User("Malinali Becerril", "MALI1998", 20, "malistar98@gmail.com"));
		user.add(new User("Erik", "123456", 23, "erik@gmail.com"));
	}

	public ArrayList<User> getUsers(){
		return this.user;
	}

	public ArrayList<Item> getStock(){
		return item;
	}

	public ArrayList<Item> getALItem(){
			return item;
	}
}